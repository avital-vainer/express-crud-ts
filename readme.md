# Express CRUD - Json file
## Exercise:
Create a new TS based express rest API
It needs to implement a users router and will handle incoming requests to Create, Update and Delete a user
It will also allow us to Read a single user and return a list of all existing users
Our DB will be a JSON file
We will also implement a log plain text file that will log each request made to the server in a single line and will include the request http method, the path and a timestamp