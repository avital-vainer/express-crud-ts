import { HttpException } from "./HttpException.js";

 
export default class UrlNotFoundException extends HttpException {
  constructor(wrongPath: string) {
    super(404, `Url ${wrongPath} was not found 😔`);  // pass the status and eror-msg
  }
}