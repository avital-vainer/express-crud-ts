import fs from 'fs/promises';
import log from '@ajar/marker';
import { NextFunction, Request, Response } from 'express';
import UrlNotFoundException from '../exceptions/UrlNotFoundException.js';
import { IErrorResponse } from '../utils.js';
import { HttpException } from '../exceptions/HttpException.js';
 

export function urlNotFoundHandler(req: Request, res: Response, next: NextFunction) {
    next(new UrlNotFoundException(req.originalUrl));
}

export const errorLogger = (logFilePath: string) => {

    return async (err: HttpException, req: Request, res: Response, next: NextFunction) => {
        await fs.appendFile(logFilePath, `# ${req.id} --> ${err.status} : ${err.message} >> ${err.stack} \n`, "utf-8");
        next(err);
    };
};

export function printAndRespondError(error: HttpException, req: Request, res: Response, next: NextFunction) {
  const errRes: IErrorResponse = { 
    status: error.status || 500, 
    msg: error.message || 'Something went wrong',
    stack: error.stack
  };

  log.red(`Error: ${errRes.msg}`);
  res.status(errRes.status).json(errRes);
}