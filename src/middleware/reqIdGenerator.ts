import { NextFunction, Request, Response } from "express";


export function generateReqID(req: Request, res: Response, next: NextFunction) {
    req.id = Math.random().toString(36).substring(2);  // !!!! uuidv4();  !!!!!
    next();
}