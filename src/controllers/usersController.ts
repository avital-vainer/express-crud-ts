import { UserNotFoundException } from "../exceptions/UserNotFoundException.js";
import userService from "../services/usersService.js";
import { Request, Response, NextFunction, RequestHandler } from "express";
import usersService from "../services/usersService.js";


// ====== functions to handle the http requests ======

export class UserController {
  loadUsersFromDB: RequestHandler = async (req, res, next) => {
    req.users = await usersService.getAllUsers();
    next();
  };

  getAllUsers: RequestHandler = (req, res) => {
    res.status(200).json(req.users);
  };

  getUserByID: RequestHandler = (req, res, next) => {
    let user = usersService.getByID(req.params.userId, req.users);
    if (user !== undefined) res.status(200).json(user);
    else next(new UserNotFoundException(req.params.userId));
    // else res.status(422).send("the requested user doesn't exist");
  };

  getUserByName: RequestHandler = (req, res, next) => {
    let user = usersService.getByName(req.params.firstName, req.params.lastName, req.users);

    if (user !== undefined) res.status(200).json(user);
    else next(new UserNotFoundException(req.params.firstName + " " + req.params.lastName));
    // else res.status(422).send("the requested user doesn't exist");
  };

  createNewUser: RequestHandler = async (req, res) => {
    req.users = await usersService.createUser(req.body, req.users);
    res.status(200).json(req.body);
  };

  updateUser: RequestHandler = async (req, res, next) => {
    let user = await userService.updateUser(req.params.userId, req.body, req.users);
    if(user) res.status(200).json(user);
    else next(new UserNotFoundException(req.params.userId));

    // let idx = req.users.findIndex((user) => user.id === req.params.userId);
    // if (idx !== -1) {
    //   req.users[idx] = { id: req.params.userId, ...req.body };
    //   // console.log("user = ", req.users[idx]);
    //   // console.log("users[] = ", req.users);
    //   await updateUsersDB(dbFilePath, req.users);
    //   res.status(200).json(req.users[idx]);
    // } else {
    //   next(new UserNotFoundException(req.params.userId));
    //   // res.status(422).send("the requested user doesn't exist");
    // }
  };

  deleteUser: RequestHandler = async (req, res, next) => {
    let isDeleted = await userService.deleteUser(req.params.userId, req.users);

    if(isDeleted) res.status(200).send("user deleted successfully!");
    else next(new UserNotFoundException(req.params.userId));

    // let idxToDelete = req.users.findIndex(
    //   (user) => user.id === req.params.userId
    // );

    // if (idxToDelete !== -1) {
    //   // console.log("user = ", req.users[idxToDelete]);
    //   req.users.splice(idxToDelete, 1);
    //   await updateUsersDB(dbFilePath, req.users);
    //   // console.log("users[] = ", req.users);
    //   res.status(200).send("user deleted successfully!");
    // } else {
    //   next(new UserNotFoundException(req.params.userId));
    //   // res.status(422).send("the requested user doesn't exist");
    // }
  };
}
