import { IUser } from "../utils.js";
import fs from "fs/promises";

export class UsersModel {
    async getUsersFromDB(db: string): Promise<IUser[]> {
        try {
            let users = await fs.readFile(db, "utf-8");
            return JSON.parse(users);
        } catch (error) {
            await fs.writeFile(db, "", "utf-8");
            return [];
        }
    }
    
    async updateUsersDB(db: string, state: IUser[]) {
        await fs.writeFile(db, JSON.stringify(state, null, 2), "utf-8");
    }
}