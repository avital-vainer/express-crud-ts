// singleton class

import { dbFilePath, generateUniqueId, IUser, updateUsersDB } from "../utils.js";
import { UsersModel } from "../model/usersModel.js";


class UserService {
    usersModel: UsersModel = new UsersModel();

    async getAllUsers() {
        const users = await this.usersModel.getUsersFromDB(dbFilePath);
        return users;
    }

    async getByID(id: string, users: IUser[]) {
        let user = users.find((user) => user.id === id);
        return user;
    }

    async getByName(first: string, last: string, users: IUser[]) {
        let user = users.find((user) => user.firstName === first && user.lastName === last);
        return user;
    }

    async createUser(newUser: Omit<IUser, "id">, users: IUser[]) {
        const id = generateUniqueId();
        const user: IUser = { id, ...newUser };
        // console.log("user = ", user);
        users.push(user);
        // console.log("users[] = ", req.users);
        await this.usersModel.updateUsersDB(dbFilePath, users);
        return users;
    }

    async updateUser(id: string, updatedUser: Omit<IUser, "id">, users: IUser[]) {
        let idx = users.findIndex((user) => user.id === id);
        if (idx !== -1) {
          users[idx] = { id, ...updatedUser};
          await updateUsersDB(dbFilePath, users);
          return users[idx];
        } else {
          return undefined;
        }
    }

    async deleteUser(id: string, users: IUser[]) {
        let idxToDelete = users.findIndex((user) => user.id === id);
      
          if (idxToDelete !== -1) {
            users.splice(idxToDelete, 1);
            await updateUsersDB(dbFilePath, users);
            return true; // deleted successfully
          } else return false;

    }
}


let instance = new UserService();
export default instance;