import fs from "fs/promises";


export interface IErrorResponse {
    status: number;
    msg: string;
    stack?: string;
}


export interface IUser {
    id: string;
    firstName: string;
    lastName: string;
    age: number;
    city: string;
}

declare global {
    namespace Express {
        interface Request {
            id: string;
            users: IUser[];
        }
    }
}

export const dbFilePath = `${process.cwd()}/db/UsersDB.json`;
export const reqLogFilePath = `${process.cwd()}/requests.log`;
export const errorLogFilePath = `${process.cwd()}/errors.log`;

export const generateUniqueId = () => Math.random().toString(36).slice(-5);

export async function getUsersFromDB(db: string): Promise<IUser[]> {
    try {
        let users = await fs.readFile(db, "utf-8");
        return JSON.parse(users);
    } catch (error) {
        // console.log(error);
        await fs.writeFile(db, "", "utf-8");
        return [];
    }
}

export async function updateUsersDB(db: string, state: IUser[]) {
    await fs.writeFile(db, JSON.stringify(state, null, 2), "utf-8");
}