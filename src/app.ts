import express, { Request, Response, NextFunction } from 'express';
import log from '@ajar/marker';
import morgan from 'morgan';
import apiRouter from "./routers/api.js";
import { reqLogger } from './middleware/logger.js';
import { errorLogFilePath, reqLogFilePath } from './utils.js';
import { errorLogger, printAndRespondError, urlNotFoundHandler } from './middleware/errorHandlers.js';
import { generateReqID } from './middleware/reqIdGenerator.js';

const { PORT, HOST } = process.env;

const app = express();
 
app.use( morgan('dev') );
app.use( express.json() );
app.use(reqLogger(reqLogFilePath));
app.use(generateReqID);

app.use("/api", apiRouter);

app.use(urlNotFoundHandler);
app.use(errorLogger(errorLogFilePath));
app.use(printAndRespondError);


app.listen(Number(PORT), HOST as string,  ()=> {
    log.magenta(`🌎  listening on`,`http://${HOST}:${PORT}`);
});