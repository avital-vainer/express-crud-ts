import express, { Router, Request, Response, NextFunction } from "express";
import { dbFilePath, IUser, generateUniqueId, getUsersFromDB, updateUsersDB } from "../utils.js";
import { UserNotFoundException } from "../exceptions/UserNotFoundException.js";
import { UserController } from "../controllers/usersController.js";

const usersController = new UserController();
const router: Router = express.Router();
export default router;


router.use(express.json());

router.use(usersController.loadUsersFromDB);

// get all users        // http://localhost:3030/api/users/
router.get("/", usersController.getAllUsers);

// get user by id       // http://localhost:3030/api/users/b58fj
router.get("/:userId", usersController.getUserByID);

// get user by name     // http://localhost:3030/api/users/Moshe/Levi
router.get("/:firstName/:lastName", usersController.getUserByName);

// create new user     // http://localhost:3030/api/users/
router.post("/", usersController.createNewUser);

// update a user       // http://localhost:3030/api/users/b58fj
router.put("/:userId", usersController.updateUser);

// delete a user       // http://localhost:3030/api/users/a8f4j
router.delete("/:userId", usersController.deleteUser);
