import express, { Router, Request, Response, NextFunction } from 'express';
import usersRouter from "./users.js";


const router: Router = express.Router();
export default router;

router.use("/users", usersRouter);